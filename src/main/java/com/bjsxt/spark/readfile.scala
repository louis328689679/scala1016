package com.bjsxt.spark

import java.io.{BufferedReader, InputStreamReader}
import java.util.zip.ZipInputStream

import org.apache.spark.input.PortableDataStream
import org.apache.spark.{SparkConf, SparkContext}

object ReadZip {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setMaster("local").setAppName("readZip")
    val sc = new SparkContext(conf)


    val dataAndPortableRDD = sc.binaryFiles("./words.zip")

    val dataRDD = dataAndPortableRDD.flatMap { case (name: String, content: PortableDataStream) =>
      val zis = new ZipInputStream(content.open)
      Stream.continually(zis.getNextEntry)
        .takeWhile(_ != null)
        .flatMap { _ =>
          val br = new BufferedReader(new InputStreamReader(zis))
          Stream.continually(br.readLine()).takeWhile(_ != null)
        }
    }

    val result = dataRDD.take(20)

    for (i <- result) {
      println(i)
    }

    sc.stop()


  }

}
