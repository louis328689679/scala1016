package com.bjsxt.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object WordCount {
  def main(args: Array[String]): Unit = {
//    val conf = new SparkConf()
//    conf.setMaster("local").setAppName("wc")
//    val sc = new SparkContext(conf)
//    val lines = sc.textFile("./words")
//    
//    val words = lines.flatMap { line => {line.split(" ")} }
//    val pairWords = words.map { word => new Tuple2(word,1) }
//    val result = pairWords.reduceByKey((a:Int,b:Int)=>{a+b})
//    val result1 = result.sortBy(tuple=>{tuple._2},false)
//    result1.foreach(tuple =>{println(tuple)})
    val conf = new SparkConf()
    conf.setMaster("local").setAppName("wc")
    val sc = new SparkContext(conf)
    sc.textFile("./words").flatMap( _.split(" ")).map((_,1)).reduceByKey(_+_).sortBy(_._2,false).foreach(println)

  }
}