package com.bjsxt.spark.scala

/**
 * 1. trait 不可以传参
 * 2. 类继承多个trait的时候，第一个关键字用extends，之后使用with
 */
trait Speak{
  val tp = "speak"
  def speak(name:String)={
    println(name+" is speaking...")
  }
}
trait Read{
  val tp = "read"
	def read(name:String)={
		println(name+" is reading...")
	}
}

class Person1 extends Speak with Read{
  override val tp = "person"
}
object Lesson_Trait {
  def main(args: Array[String]): Unit = {
    val p = new Person1()
    p.speak("zhangsan")
    p.read("lisi")
    println(p.tp)
  }
}