package com.bjsxt.spark.scala

object Lesson_Match {
  def main(args: Array[String]): Unit = {
    val tuple = (1,1.1,"abc",'d',12.32f)
    val iter = tuple.productIterator
    while(iter.hasNext){
      TestMatch(iter.next())
    }
  }
  
  def TestMatch(x:Any)=
    x match {
      case 1=>println("value is 1")
      case i:Int=>println("type is Int")
      case x:String=>println("type is String")
      case c:Char =>println("type is Char")
      case d:Double => println("type is Double")
      case _=>println("no match...")
    }
  
  
  
}