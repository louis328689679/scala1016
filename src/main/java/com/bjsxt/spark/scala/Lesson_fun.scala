package com.bjsxt.spark.scala

import java.util.Date
import java.text.SimpleDateFormat


object Lesson_fun {
  def main(args: Array[String]): Unit = {
    /**
     * 1.函数定义
     * 
     * 注意：
     * 1.定义函数的时候参数的类型不能省略
     * 2.方法的返回类型可以省略，默认方法将方法体中最后一行的计算结果返回。如果写return 返回方法的值，那么要显示声明方法的返回类型。
     * 3.scala中方法中方法体逻辑如果可以一行搞定，可以将外面的"{...}"去掉
     * 4.定义函数时，如果不写“=” ，无论方法体计算的结果是什么都会被丢弃，返回Unit。
     */
    
//    def max(a:Int,b:Int)= {
//      if(a>b){
//         a
//      }else{
//         b
//      }
//    }
//    def max(a:Int,b:Int)= if(a>b) a else  b
//    def max(a:Int,b:Int):Int = {
//      if(a>b){
//         a
//      }else{
//         b
//      }
//    }
//    println(max(10,23))
    
    /**
     * 2.递归函数
     * 递归函数一定要显式声明函数的返回类型
     */
//    def fun(num:Int):Int={
//      if(num==1){
//        1
//      }else{
//        num*fun(num-1)
//      }
//    }
    
//    println(fun(3))
    /**
     * 3.有多个参数的函数
     */
    
//    def fun(a:Int*)= {
//      a.foreach {x=>{ println(x)} }
//      a.foreach {println(_) }
//      a.foreach {println}
//     for(i<-a){
//       println(i)
//     }
//    }
    
//    fun(1,2,3,4,5)
    
    /**
     * 4.参数有默认值的函数
     */
//    def fun(a:Int=10,b:Int=20) = {
//      a+b
//    }
//    
//    println(fun(11,21))
//    
    /**
     * 5.匿名函数
     * 匿名函数不能显式的声明函数返回类型
     * 
     */
//    val fun = ()=>{
//      println("hello world!")
//    }
//    
//    fun()
//    val fun = (a:Int,b:Int)=>{
//      a+b
//    }
//    
//    println(fun(1,2))
    
    /**
     * 6.嵌套函数
     * 
     */
    
//    def fun(a:Int) = {
//      
//      def fun1(num:Int) :Int={
//        if(num==1){
//          1
//        }else{
//          num*fun1(num-1)
//        }
//      }
//      
//      fun1(a)
//    }
//    
//    println(fun(5))
    /**
     * 7.偏应用函数
     * 
     */
//    def showLog(date:Date,log:String) = {
//      println("date is "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date)+" , log is "+log)
//    }
//    
//    val date = new Date()
//    
//    showLog(date, "a")
//    showLog(date, "b")
//    showLog(date, "c")
//    
//    val fun = showLog(date:Date, _:String)
//    
//    fun("aaa")
//    fun("bbb")
//    fun("ccc")
    
    /**
     * 8.高阶函数
     * 	1).函数的参数的是函数
     * 	2).函数的返回是函数
     * 	3).函数的参数和返回都是函数
     */
    
//    def fun(a:Int,b:Int):Int = {
//      a+b
//    }
//    
//    //函数的参数是函数
//    def fun1(f:(Int,Int)=>Int) = {
//      f(1,2)
//    }
//    
//    println(fun1(fun))
    
    
    /**
     * 函数的返回是函数
     * 1.要写上函数的返回类型
     */
//    def fun(a:Int,b:Int) :(String,String)=>String = {
//      def fun1(s1:String,s2:String) = {
//        s1+"~"+s2+"@"+a+"#"+b
//      }
//      fun1
//    }
//    
//    println(fun(10,20)("hello","bjsxt"))//hello~bjsxt@10#20
    
    //函数的参数是函数，函数返回是函数
//    def fun(f:(Int,Int)=>Int,a:String) :(String,String)=>String = {
//      val value = f(1,2)
//      def fun1(s1:String,s2:String):String = {
//        s1+"~"+s2+"!"+value+"#"+a
//      }
//     fun1
//    }
//    val www = fun((a:Int,b:Int)=>{a+b+100},"abc")
//    www("jjj","sss")
//    println(fun((a:Int,b:Int)=>{a+b+100},"abc")("hello","bjsxt"))//hello~bjsxt!103#abc
    
    
    /**
     * 9.柯里化函数
     * 高阶函数的简化版
     */
    
    def fun(a:Int,b:Int)(c:Int,d:Int) = {
      a+b+c+d
    }
    println(fun(1,2)(3,4))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  }
}