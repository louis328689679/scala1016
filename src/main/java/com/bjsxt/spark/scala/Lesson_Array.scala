package com.bjsxt.spark.scala

object Lesson_Array {
  def main(args: Array[String]): Unit = {
    //创建
//    val arr = Array[Int](1,2,3)
//    val arr1 = new Array[Int](3)
//    arr1(0) = 4
//    arr1(1) =5
//    arr1(2) = 6
    //遍历
//    for(elem<-arr){
//      println(elem)
//    }
    //arr1.foreach{elem=>{println(elem)}}
    /**
     * 创建二维数组
     */
//    val array = Array[Array[String]](
//      Array[String]("a","b","c"),    
//      Array[String]("d","e","f")
//    )
    
//    for(arr <- array;elem<-arr){
//      println(elem)
//    }
//    array.foreach { arr => {
//      arr.foreach { println }
//    } }
    
//    val array = new Array[Array[Int]](3)
//    
//    array(0) = Array[Int](1,2,3)
    
//      val arr1 = Array(1,2,3)
//    	val arr2 = Array(4,5,6)
//    val arr3 =  Array.concat(arr1,arr2)
//    arr3.foreach(println)
    
    val arr = Array.fill(5)("bjsxt")
    arr.foreach{println}
    
    
  }
}