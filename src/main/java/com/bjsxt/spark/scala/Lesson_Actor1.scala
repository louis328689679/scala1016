package com.bjsxt.spark.scala

import scala.actors.Actor

case class Msg(actor:Actor,message:String)

class MyActor1 extends Actor {
  def act(): Unit = {
    while(true){
    	receive {
      	case s:Msg =>{
      		if(s.message.equals("hello~")){
      			println("hello~")
      			s.actor ! "hi~"
      		}else if(s.message.equals("could we have a date?")){
      			println("could we have a date?")
      			s.actor ! "ok~"
      		}
      	}
      	case _=>println("no match...")
    	}
    }
  }
}
class MyActor2(actor:Actor) extends Actor {
  actor ! Msg(this,"hello~")
	def act(): Unit = {
    while(true){
    	receive {
      	case s:String=>{
      		if(s.equals("hi~")){
      			println("hi~")
      			actor ! Msg(this,"could we have a date?")
      		}else if(s.equals("ok~")){
      			println("ok~")
      			println("Let's go...")
      		}
      	}
      	case _=>println("no match...")
    	}
    }
	}
}

object Lesson_Actor1 {
  def main(args: Array[String]): Unit = {
    val actor1 = new MyActor1()
    val actor2 = new MyActor2(actor1)
    actor1.start()
    actor2.start()
    
  }
}