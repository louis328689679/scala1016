package com.bjsxt.spark.scala

/**
 * 1.scala中定义变量使用var，定义常量用val,不能给常量重新赋值。变量可以省略类型，会自动推断类型。
 * 2.scala中一行后面分号可以省略，会有自动分号推断,如果一行中有多个语句，分号不能省略。
 * 3.类和变量，对象定义建议使用驼峰命名法
 * 4.object，对象，相当于java中的单例，里面定义的全是静态的，object不可以传参。
 * 5.class可以传参。只要传参就有了默认构造。类中的属性默认有getter，setter。重写构造第一行要先调用默认构造
 * 6.scala中当new一个类的时候，除了方法不执行，其他都执行。
 * 7.scala中属性的默认访问级别相当于java中public。
 * 8.如果在一个scala文件中，object的名称和class的名称相同，那么这个类叫做这个对象的伴生类，这个对象叫做这个类的伴生对象，他们之间可以互相访问私有变量
 * 9.scala中同一个包下的所有的scala文件相当于写在了一个大的文本里面。
 *  
 */
class Person(xname:String,xage:String){
  private var name = xname
  val age = xage
  var gender = 'm'
  
  println("*******************")
  
  def this(yname:String,yage:String,ygender:Char){
    this(yname,yage)
    this.gender = ygender
  }
  
  def sayName() = {
    println("name is "+Person.name)
  }
  
}

object Person {
  val name = "dongshi"
  
//  println("++++++++++++++++++++")
  
  def main(args: Array[String]): Unit = {
//    val a  =100
//    println(a)
    
//    val person = new Person("zhangsan","18")
//    person.name = "lisi"
//    println(person.name)
//    println(person.gender)
//    val person1 = new Person("xishi","20",'f')
//    println(person1.gender)
    
//    val person = new Person("zhangsan","18")
//    println(person.name)
//    person.sayName()
   
//    println(1 to (10,2))
//    println(1 until (10,3))
    
    /**
     * for
     */
//    for(i<-1 to 10){
//      for(j<-1 until 10){
//        
//      }
//    }
    
//    for(i<-1 to 9 ;j<- 1 to 10){
//      if(i>=j){
//        print(i+"*"+j+"="+i*j+"\t")
//      }
//      if(i==j){
//        println()
//      }
//    }
    
//    for(i<-1 to 100;if(i%2==0);if(i==98)){
//      println(i)
//    }
    
//    val v = for(i<-1 to 20;if(i%2==0)) yield i
//    
//    println(v)
    
    /**
     * while do...while()
     */
    var sum =0 
    while(sum<100){
      println("第"+sum+"次求婚。。。")
//      sum = sum+1
      sum +=1
    }
    sum = 0
    do{
       println("第"+sum+"次求婚...")
       sum +=1
    }while(sum<100)
    
    
  }
}