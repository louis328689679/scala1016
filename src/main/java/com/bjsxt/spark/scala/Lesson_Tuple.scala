package com.bjsxt.spark.scala

object Lesson_Tuple {
  def main(args: Array[String]): Unit = {
    val tuple1 = new Tuple1(1)
    val tuple2 = new Tuple2(1,2)
    val tuple3 = Tuple3(1,2,3)
    val tuple22 = (1,2,3,4,5,'c',7,8,9,"abc",11,12,13,14,15,16,17,18,19,20,21,22)
    println(tuple22.toString())
    
    println(tuple22._10)
    //遍历
//    val iter = tuple22.productIterator
//    while(iter.hasNext){
//      println(iter.next())
//    }
    
  }
}