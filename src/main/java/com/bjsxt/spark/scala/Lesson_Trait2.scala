package com.bjsxt.spark.scala

trait Equ {
  def isEqu(point:Any) :Boolean
  def isNotEqu(point:Any) : Boolean=  !isEqu(point)
}

class Point(xx:Int, xy:Int) extends Equ {
  val x = xx
  val y = xy

  def isEqu(point: Any): Boolean = {
    val result = point.isInstanceOf[Point]&&point.asInstanceOf[Point].x==this.x
    result
  }
}

object Lesson_Trait2 {
  def main(args: Array[String]): Unit = {
    val p1 = new Point(1,2)
    val p2 = new Point(1,3)
   println(p1.isEqu(p2))
   println(p1.isNotEqu(p2))
    
  }
}