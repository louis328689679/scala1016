package com.bjsxt.spark.scala

import scala.actors.Actor

class MyActor extends Actor {
  def act(): Unit = {
    receive {
      case s:String=>println("message is "+s)
      case _=>println("no match...")
    }
  }
}

object Lesson_Actor {
  def main(args: Array[String]): Unit = {
    val actor = new MyActor()
    actor.start()
    actor ! 1000
  }
}