package com.bjsxt.spark.scala

case class Person2(name:String)

object Lesson_CaseClass {
  def main(args: Array[String]): Unit = {
    val person =   Person2("zhangsan")
    val person1 =   Person2("zhangsan")
    println(person==person1)
  }
}