package com.bjsxt.spark.com.spark.unzip.csv

import java.io._
import java.net.URLDecoder;
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


/**
  * Created by 21559 on 2018/4/10.
  */
object ToCSV {


  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("Second").setMaster("local[2]")
    val sc = new SparkContext(conf)
    val filePath = "F:\\data\\xsv\\abc\\2014\\2014\\12\\01"
     val files: Iterator[File] = subdirs2(new File(filePath));
   //新建csv文件
 // val writer: PrintWriter = new PrintWriter(new File("D:\\data\\xcx\\track_20171201\\track_20171201\\track\\2017\\12\\01\\1.csv" )) //名字自己起
    //定义文件名格式并创建
   val csvFile = File.createTempFile("S01", ".csv", new File("G:\\data\\xsv\\S02\\2.csv"));

    // UTF-8使正确读取分隔符","
    //如果生产文件乱码，windows下用gbk，linux用UTF-8
    val  csvFileOutputStream: BufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
     csvFile), "UTF-8"), 1024);
    files.foreach(x=>save(csvFileOutputStream,x.getName,sc,filePath))
    sc.stop()   // 释放资源

  }

  def save(writer:BufferedWriter ,fileName:String, sc:SparkContext ,filePath:String) ={

    val name = filePath+ "//" +fileName
    val lines: RDD[String] = sc.textFile(name)   // 读取本地文件
    val words: RDD[Array[String]] = lines.map(x=>x.split(":"))
    var date: String = words.map(x=>x(2).split("/")(0)).take(1)(0) //获取时间
    var chepai = URLDecoder.decode(fileName.split("\\.")(0).split("_")(1),"GBK") //获取车牌，此处需转码
    var list = List(date,chepai)
    val words1: RDD[List[String]] = words.map(x=>x.toList).map(x=>(x ++ list))
    //savaRdd 暂时没写出来
   words1.collect().foreach(x=>writeRow(x,writer))


  }
  def subdirs2(dir: File): Iterator[File] = {
    val d = dir.listFiles.filter(_.isDirectory)
    val f = dir.listFiles.filter(_.isFile).toIterator
    f ++ d.toIterator.flatMap(subdirs2 _)
  }

  /**
    * 写一行数据方法
    *
    * @param row
    * @param csvWriter
    * @throws IOException
    */
  def  writeRow( row : List[String],  csvWriter:BufferedWriter) {

    var a = 0
    var sb = new StringBuffer()
    for (a <- row) {

       sb = sb.append("\"").append(a).append("\",")

    }
    csvWriter.write(sb.toString)
    csvWriter.newLine();

  }
}
