package com.bjsxt.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
/**
 * cache默认将数据持久化到内存中,cache = persist(StorageLevel.MEMORY_ONLY())
 * persist:可以手动指定持久化级别
 * 	MEMORY_ONLY
 *  MEMORY_ONLY_SER
 *  MEMORY_AND_DISK_SER
 *  MEMORY_AND_DISK：内存放不下再放磁盘
 *  "_2"代表数据有副本，一般不考虑这种级别。
 *
 * cache执行注意：(持久化的单位是partition)
 * 1.cache和persist都是懒执行，需要Action算子触发执行。
 * 2.对一个RDD进行cache/persist之后，可以赋值给一个变量，下次直接使用这个变量就是使用的持久化的数据。
 * 3.cache/persist后面不能直接紧跟Action算子
 *
 * checkPoint()
 *
 *
 *
 *
 *
 */
public class WordCount1 {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf();
        conf.setMaster("local").setAppName("test");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> linesRDD = sc.textFile("./NASA_access_log_Aug95");

//		linesRDD = linesRDD.cache();
		linesRDD = linesRDD.persist(StorageLevel.MEMORY_AND_DISK());
//		linesRDD = linesRDD.persist(new StorageLevel(true,false,true,false,5));
        sc.setCheckpointDir("./ck");
        linesRDD.checkpoint();
        long start = System.currentTimeMillis();
        long count = linesRDD.count();
        long end = System.currentTimeMillis();
        System.out.println("first -- count = "+count+", time = "+(end-start)+" ms");

        long start1 = System.currentTimeMillis();
        long count1 = linesRDD.count();
        long end1 = System.currentTimeMillis();
        System.out.println("second -- count = "+count1+", time = "+(end1-start1)+" ms");

        sc.stop();
    }
}
